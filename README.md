# Spade LSP Client for VSCode

## Features

The following features are _planned_:

- Swim projects
- Semantic highlighting
- Subdiagnostics
    - Secondary labels
    - Quick fixes (suggestions)
- Go to definition etc.
- Rename item
- ...

### Diagnostics

![Example diagnostic](/screenshots/diagnostics.png)

## Requirements

For now you'll have to build the language server yourself (and keep it up to date)
by building the
[Spade language server](https://gitlab.com/spade-lang/spade-language-server)
from source. For example:

```sh
$ git clone https://gitlab.com/spade-lang/spade-language-server
$ cd spade-language-server
$ cargo build
```

You'll also need to set the path to the compiled binary in the settings (see below).

## Extension Settings

This extension contributes the following settings:

* `spade-language-server.language-server-binary`: Path to the language server
  binary. For example,
  `/home/gustav/spade/spade-language-server/target/debug/spade-language-server`
